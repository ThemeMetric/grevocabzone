if (typeof iwtValidationDeclared == 'undefined') {
	var iwtValidationDeclared = false;

	var iwtValidate = (function() {
		//some forms have mutiple of these
		//closure so it will only run once
		return function() {
			if (!iwtValidationDeclared) {
				iwtValidationDeclared = true;
				if (typeof $ != 'undefined') {
					$(document).ready(function(){
						$('form').each(function(){
							if ($(this).attr('action').indexOf('\\signup')) {
								$(this).submit(function(event){
									$(this).find('div input[type=text], div input[type=email]').each(function(){
										var textLabel = $(this).siblings('label');
										var labelText;
										if (textLabel.length > 0) {
											labelText = $(textLabel).text().replace(':', '');
										}
										else {
											//label was not found
											var nameAttr = $(this).attr('name');
											newlabelText = nameAttr.substr(0,1).toUpperCase()+nameAttr.substr(1);
											labelText = newlabelText;
										}

										if (typeof labelText == 'undefined') {
											labelText = "value";
										}

										if (!$(this).val()) {
											event.preventDefault();
											alert("Sorry, you didn't enter your "+labelText+".");
											$(this).focus();
											return false;
										}
									});

									return true;
								});
							}
						});
					});
				}
			}
		}
	})();

	iwtValidate();
}