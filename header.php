<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package grevocabzone
 */
global $grevocabzone;
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href='https://fonts.googleapis.com/css?family=Crimson+Text:400,400italic,700,700italic|Open+Sans:400,300,700|Marvel' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="<?php echo $grevocabzone['favicon'][url] ; ?>" />
 <!-- ******************************************************************** -->
    <!-- * Custom css code ************************************ -->
    <!-- ******************************************************************** -->
    <style type="text/css">
    <?php if ( (isset($grevocabzone['custom-css'])) && ($grevocabzone['custom-css'] != "") ) : ?>
		<?php echo $grevocabzone['custom-css']; ?>
    <?php endif; ?>
    
    </style>
    <!-- ******************************************************************** -->
    <!-- * Google analytics Code ************************************ -->
    <!-- ******************************************************************** -->
 
    
    <?php if ( (isset($grevocabzone['analytics_js'])) && ($grevocabzone['analytics_js'] != "") ) : ?>
		<?php echo $grevocabzone['analytics_js']; ?>
    <?php endif; ?>
    
<script src="<?php echo get_template_directory_uri() ; ?>./assets/js/modernizr.js"></script>
  <script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(t,e,n){function r(n){if(!e[n]){var o=e[n]={exports:{}};t[n][0].call(o.exports,function(e){var o=t[n][1][e];return r(o||e)},o,o.exports)}return e[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<n.length;o++)r(n[o]);return r}({1:[function(t,e,n){function r(){}function o(t,e,n){return function(){return i(t,[(new Date).getTime()].concat(u(arguments)),e?null:this,n),e?void 0:this}}var i=t("handle"),a=t(2),u=t(3),c=t("ee").get("tracer"),f=NREUM;"undefined"==typeof window.newrelic&&(newrelic=f);var s=["setPageViewName","setCustomAttribute","finished","addToTrace","inlineHit"],p="api-",l=p+"ixn-";a(s,function(t,e){f[e]=o(p+e,!0,"api")}),f.addPageAction=o(p+"addPageAction",!0),e.exports=newrelic,f.interaction=function(){return(new r).get()};var d=r.prototype={createTracer:function(t,e){var n={},r=this,o="function"==typeof e;return i(l+"tracer",[Date.now(),t,n],r),function(){if(c.emit((o?"":"no-")+"fn-start",[Date.now(),r,o],n),o)try{return e.apply(this,arguments)}finally{c.emit("fn-end",[Date.now()],n)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(t,e){d[e]=o(l+e)}),newrelic.noticeError=function(t){"string"==typeof t&&(t=new Error(t)),i("err",[t,(new Date).getTime()])}},{}],2:[function(t,e,n){function r(t,e){var n=[],r="",i=0;for(r in t)o.call(t,r)&&(n[i]=e(r,t[r]),i+=1);return n}var o=Object.prototype.hasOwnProperty;e.exports=r},{}],3:[function(t,e,n){function r(t,e,n){e||(e=0),"undefined"==typeof n&&(n=t?t.length:0);for(var r=-1,o=n-e||0,i=Array(o<0?0:o);++r<o;)i[r]=t[e+r];return i}e.exports=r},{}],ee:[function(t,e,n){function r(){}function o(t){function e(t){return t&&t instanceof r?t:t?u(t,a,i):i()}function n(n,r,o){t&&t(n,r,o);for(var i=e(o),a=l(n),u=a.length,c=0;c<u;c++)a[c].apply(i,r);var s=f[m[n]];return s&&s.push([w,n,r,i]),i}function p(t,e){g[t]=l(t).concat(e)}function l(t){return g[t]||[]}function d(t){return s[t]=s[t]||o(n)}function v(t,e){c(t,function(t,n){e=e||"feature",m[n]=e,e in f||(f[e]=[])})}var g={},m={},w={on:p,emit:n,get:d,listeners:l,context:e,buffer:v};return w}function i(){return new r}var a="nr@context",u=t("gos"),c=t(2),f={},s={},p=e.exports=o();p.backlog=f},{}],gos:[function(t,e,n){function r(t,e,n){if(o.call(t,e))return t[e];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(t,e,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return t[e]=r,r}var o=Object.prototype.hasOwnProperty;e.exports=r},{}],handle:[function(t,e,n){function r(t,e,n,r){o.buffer([t],r),o.emit(t,e,n)}var o=t("ee").get("handle");e.exports=r,r.ee=o},{}],id:[function(t,e,n){function r(t){var e=typeof t;return!t||"object"!==e&&"function"!==e?-1:t===window?0:a(t,i,function(){return o++})}var o=1,i="nr@id",a=t("gos");e.exports=r},{}],loader:[function(t,e,n){function r(){if(!h++){var t=y.info=NREUM.info,e=s.getElementsByTagName("script")[0];if(t&&t.licenseKey&&t.applicationID&&e){c(m,function(e,n){t[e]||(t[e]=n)});var n="https"===g.split(":")[0]||t.sslForHttp;y.proto=n?"https://":"http://",u("mark",["onload",a()],null,"api");var r=s.createElement("script");r.src=y.proto+t.agent,e.parentNode.insertBefore(r,e)}}}function o(){"complete"===s.readyState&&i()}function i(){u("mark",["domContent",a()],null,"api")}function a(){return(new Date).getTime()}var u=t("handle"),c=t(2),f=window,s=f.document,p="addEventListener",l="attachEvent",d=f.XMLHttpRequest,v=d&&d.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:d,REQ:f.Request,EV:f.Event,PR:f.Promise,MO:f.MutationObserver},t(1);var g=""+location,m={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-963.min.js"},w=d&&v&&v[p]&&!/CriOS/.test(navigator.userAgent),y=e.exports={offset:a(),origin:g,features:{},xhrWrappable:w};s[p]?(s[p]("DOMContentLoaded",i,!1),f[p]("load",r,!1)):(s[l]("onreadystatechange",o),f[l]("onload",r)),u("mark",["firstbyte",a()],null,"api");var h=0},{}]},{},["loader"]);</script>
  
<script type="text/javascript">
	if (typeof jQuery === 'undefined'){
		document.write("<scr" + "ipt type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js\"></scr" + "ipt>");
	}
</script>
 
 
<!-- Google Tag Manager -->
<noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-N34QXB" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'http://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-N34QXB');</script>
<!-- End Google Tag Manager -->
<!-- Kissmetrics -->
<script type="text/javascript">
  var _kmq = _kmq || [];
  var _kmk = _kmk || 'ad1982fce875af9fb737ff3b1efa85e0e969bd59';
  function _kms(u){
    setTimeout(function(){
      var d = document, f = d.getElementsByTagName('script')[0],
      s = d.createElement('script');
      s.type = 'text/javascript'; s.async = true; s.src = u;
      f.parentNode.insertBefore(s, f);
    }, 1);
  }
  _kms('//i.kissmetrics.com/i.js');
  _kms('//scripts.kissmetrics.com/' + _kmk + '.2.js');
  </script>
<!-- End Kissmetrics -->


<!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=61,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->
  <script type="text/javascript" src="<?php echo get_template_directory_uri() ; ?>./assets/js/iwtcore.js"></script>

  <script type="text/javascript">
    setTimeout(function(){var a=document.createElement("script");
    var b=document.getElementsByTagName("script")[0];
    a.src=document.location.protocol+"http://script.crazyegg.com/pages/scripts/0010/1503.js?"+Math.floor(new Date().getTime()/3600000);
    a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
  </script>

 
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    
<!-- Navigation -->

<div class="navigation closed">
  <ul class="menu top">
    <li>
         <a href="<?php bloginfo('url');  ?>" target="_blank">
             <span class="part">
               <img src="<?php  echo $grevocabzone['site_logo'][url] ; ?>" alt="grevocabzone"></span>
             <span class="title">
                 <h3 style="margin-top:18px;font-weight:normal;text-transform:none;"><?php echo $grevocabzone['logo-text'] ; ?>
                 </h3>
                 <h4>
                     <img src="<?php  echo $grevocabzone['site_logo-text'][url] ; ?>" alt="" style="margin-top:7px;">
                 </h4>
             </span>
             <div class="clear"></div>
         </a>
    </li>
  </ul>
  <ul class="menu mobile-top" id="mobile-nav">
    <li>
        <span class="part">
            <img src="<?php  echo $grevocabzone['site_logo'][url] ; ?>" alt="grevocabzone"></span>
        <span class="title">
            <h4 style="text-align:left;">
                <img src="<?php  echo $grevocabzone['site_logo-text'][url] ; ?>" alt="grevocabzone" style="max-width:80%;">
            </h4>
        </span>
        <div class="burger"></div>
        <div class="clear"></div></li>
  </ul>
    

    
  <ul class="menu">
      
      
      
       <?php if ($grevocabzone['part-1-title']) :?>
      
     <li>
                
      <a href="<?php echo $grevocabzone['part-1-link'] ?>" class="tab1">
        <span class="part"><?php echo $grevocabzone['part-1-number'] ?></span>
        <span class="title copy-heavy">
          <h3><?php echo $grevocabzone['part-1-short-title'] ?></h3>
          <h4><?php echo $grevocabzone['part-1-title'] ?></h4>
        </span>
        <div class="clear"></div>
      </a>
    </li>
   
    <?php endif ?>
    
    
    <?php if ($grevocabzone['part-2-title']) :?>
            <li>
       <a href="<?php echo $grevocabzone['part-2-link'] ?>" class="tab1">
        <span class="part"><?php echo $grevocabzone['part-2-number'] ?></span>
        <span class="title copy-heavy">
          <h3><?php echo $grevocabzone['part-2-short-title'] ?></h3>
          <h4><?php echo $grevocabzone['part-2-title'] ?></h4>
        </span>
        <div class="clear"></div>
      </a>
    </li>
    <?php endif ?>
    
    
      <?php if ($grevocabzone['part-3-title']) :?>
            <li>
       <a href="<?php echo $grevocabzone['part-3-link'] ?>" class="tab1">
        <span class="part"><?php echo $grevocabzone['part-3-number'] ?></span>
        <span class="title copy-heavy">
          <h3><?php echo $grevocabzone['part-3-short-title'] ?></h3>
          <h4><?php echo $grevocabzone['part-3-title'] ?></h4>
        </span>
        <div class="clear"></div>
      </a>
    </li>
    
    <?php endif ?>
    
       <?php if ($grevocabzone['part-4-title']) :?>
        <li>
       <a href="<?php echo $grevocabzone['part-4-link'] ?>" class="tab1">
        <span class="part"><?php echo $grevocabzone['part-4-number'] ?></span>
        <span class="title copy-heavy">
          <h3><?php echo $grevocabzone['part-4-short-title'] ?></h3>
          <h4><?php echo $grevocabzone['part-4-title'] ?></h4>
        </span>
        <div class="clear"></div>
      </a>
    </li>   
    <?php endif ?>
    
    
       <?php if ($grevocabzone['part-5-title']) :?>
        <li>
       <a href="<?php echo $grevocabzone['part-5-link'] ?>" class="tab1">
        <span class="part"><?php echo $grevocabzone['part-5-number'] ?></span>
        <span class="title copy-heavy">
          <h3><?php echo $grevocabzone['part-5-short-title'] ?></h3>
          <h4><?php echo $grevocabzone['part-5-title'] ?></h4>
        </span>
        <div class="clear"></div>
      </a>
    </li>   
    <?php endif ?>
    
                <?php if ($grevocabzone['part-6-title']) :?>
        <li>
       <a href="<?php echo $grevocabzone['part-6-link'] ?>" class="tab1">
        <span class="part"><?php echo $grevocabzone['part-6-number'] ?></span>
        <span class="title copy-heavy">
          <h3><?php echo $grevocabzone['part-6-short-title'] ?></h3>
          <h4><?php echo $grevocabzone['part-6-title'] ?></h4>
        </span>
        <div class="clear"></div>
      </a>
    </li>   
    <?php endif ?>
      <?php if ($grevocabzone['part-7-title']) :?>
        <li>
       <a href="<?php echo $grevocabzone['part-7-link'] ?>" class="tab1">
        <span class="part"><?php echo $grevocabzone['part-7-number'] ?></span>
        <span class="title copy-heavy">
          <h3><?php echo $grevocabzone['part-7-short-title'] ?></h3>
          <h4><?php echo $grevocabzone['part-7-title'] ?></h4>
        </span>
        <div class="clear"></div>
      </a>
    </li>   
    <?php endif ?>
	
     </ul>
    
    
  <div class="background"></div>

  
  
</div>
  

<!-- Reading -->
<div class="chapter start">