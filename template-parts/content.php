<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package grevocabzone
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    
	<div class="entry-content">
            <div class="post-info">
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
             
                </div>
               
                <div class="post-media"> 
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog-isotope'); ?></a>

               </div>
         
               
            
                
              <div class="post-body">
              <p><?php echo wp_trim_words( get_the_content(), 50, '' ); ?></p>
              <p><a href="<?php the_permalink(); ?>" class="button">Read More</a></p>
            </div>
         
	  
             
	</div><!-- .entry-content -->

</article><!-- #post-## -->
