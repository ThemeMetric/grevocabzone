<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package grevocabzone
 */

?>


    <!-- Social shares -->
  <div class="share-container" style="display:">
    <div class="share">
      <div class="link-container">
        <a class="button facebook" href="javascript:window.open('http://www.facebook.com/sharer.php?s=100&amp;p[url]=http%253A%252F%252Fiwillteach.co%252Fhabits%252F%253Futm_source%253Dfacebook%2526amp%253Butm_medium%253Dsocial%2526amp%253Butm_campaign%253Dug-to-habits', '_blank', 'width=650,height=400');void(0);">-</a>

        <a class="button linkedin" href="javascript:window.open('http://www.linkedin.com/shareArticle?mini=true&amp;url=http%253A%252F%252Fiwillteach.co%252Fhabits%252F%253Futm_source%253Dlinkedin%2526utm_medium%253Dsocial%2526utm_campaign%253Dug-to-habits&amp;title=The+Ultimate+Guide+to+Habits&amp;summary=Imagine+how+it+would+feel+if+every+time+you+put+your+mind+to+something%2C+you+got+it+done.+You+didn%E2%80%99t+start+and+stop.+You+never+got+off+track.+You+just+finished+what+you+started+%E2%80%94+no+questions+asked.+Today%2C+I%5C%27ll+show+you+how.', '_blank', 'width=600,height=500');void(0);">-</a>

        <a class="button googleplus" href="javascript:window.open('http://plus.google.com/share?url=http%253A%252F%252Fiwillteach.co%252Fhabits%252F%253Futm_source%253Dgoogleplus%2526utm_medium%253Dsocial%2526utm_campaign%253Dug-to-habits', '_blank', 'width=500,height=500');void(0);">-</a>

        <a class="button twitter" href="javascript:window.open('http://twitter.com/share/?url=http%253A%252F%252Fiwillteach.co%252Fhabits%252F%253Futm_source%253Dtwitter%2526utm_medium%253Dsocial%2526utm_campaign%253Dug-to-habits&amp;text=Check+out+%E2%80%9CThe+Ultimate+Guide+to+Habits%E2%80%9D+from+%40ramit', '_blank', 'width=650,height=400');void(0);"></a>

        <a class="button email" href="mailto:?subject=You should check this out: The Ultimate Guide to Habits - Peak Performance Made Easy&amp;body=I just read this article from Ramit Sethi and it is filled with some great stuff -- I think you'll like it: http%3A%2F%2Fiwillteach.co%2Fhabits%2F%3Futm_source%3Demail%26utm_medium%3Dsocial%26utm_campaign%3Dug-to-habits">Email</a>
      </div>
    </div>
  </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  
  <script src="http://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/js/foundation.js"></script>

  <script>
  (function() {
    var countUp, setCount, url;
    var url = 'http://localhost/grevocabzone/';
    //url = 'http://www.iwillteachyoutoberich.com/';

    countUp = function($item) {
      return setTimeout(function() {
        var current, newCount, target;
        current = $item.attr("data-current-count") * 1;
        target = $item.attr("data-target-count") * 1;
        newCount = current + Math.ceil((target - current) / 2);
        $item.attr("data-current-count", newCount);
        $item.html(newCount);
        if (newCount < target) {
          return countUp($item);
        }
      }, 100);
    };

    setCount = function($item, count) {
      if (count == null) {
        count = null;
      }
      $item.attr("data-target-count", count);
      $item.attr("data-current-count", 0);
      return countUp($item);
    };

    //setCount($(".button.twitter"), 0);
    setCount($(".button.facebook"), 2205);
    setCount($(".button.linkedin"), 2369);
    setCount($(".button.googleplus"), 2357);

  }).call(this);
</script>

  <script>
      if (!Modernizr.svg) {
        var imgs = document.getElementsByTagName('img');
        var endsWithDotSvg = /.*\.svg$/
        var i = 0;
        var l = imgs.length;
        for(; i != l; ++i) {
            if(imgs[i].src.match(endsWithDotSvg)) {
                imgs[i].src = imgs[i].src.slice(0, -3) + 'png';
            }
        }
    }
    $(document).foundation();
    $(document).ready(function(){
      $("#mobile-nav").click(function() {
          $(".navigation").toggleClass("closed").toggleClass("opened");
      });
      $("#signup-form").click(function() {
          $(".signup-form").toggleClass("goodbye").toggleClass("hello");
      });
      $(".tab1").click(function() {
          $("#tab1").toggleClass("hide").toggleClass("show");
          $("#tab2").removeClass("show").addClass("hide");
          $("#tab3").removeClass("show").addClass("hide");
          $("#tab4").removeClass("show").addClass("hide");
          $("#tab5").removeClass("show").addClass("hide");
      });
      $(".tab2").click(function() {
          $("#tab1").removeClass("show").addClass("hide");
          $("#tab2").toggleClass("hide").toggleClass("show");
          $("#tab3").removeClass("show").addClass("hide");
          $("#tab4").removeClass("show").addClass("hide");
          $("#tab5").removeClass("show").addClass("hide");
      });
      $(".tab3").click(function() {
          $("#tab1").removeClass("show").addClass("hide");
          $("#tab2").removeClass("show").addClass("hide");
          $("#tab3").toggleClass("hide").toggleClass("show");
          $("#tab4").removeClass("show").addClass("hide");
          $("#tab5").removeClass("show").addClass("hide");
      });
      $(".tab4").click(function() {
          $("#tab1").removeClass("show").addClass("hide");
          $("#tab2").removeClass("show").addClass("hide");
          $("#tab3").removeClass("show").addClass("hide");
          $("#tab4").toggleClass("hide").toggleClass("show");
          $("#tab5").removeClass("show").addClass("hide");
      });
      $(".tab5").click(function() {
          $("#tab1").removeClass("show").addClass("hide");
          $("#tab2").removeClass("show").addClass("hide");
          $("#tab3").removeClass("show").addClass("hide");
          $("#tab4").removeClass("show").addClass("hide");
          $("#tab5").toggleClass("hide").toggleClass("show");
      });
    });
//    $(function() {
//      $( "." ).accordion({
//        heightStyle: "content"
//      });
//    });
    // window.addEventListener("load",function() {
    //   setTimeout(function(){
    //    window.scrollTo(0, 0);
    //    }, 0);
    // });
    $('a#inpage').click(function(){
      $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top
        }, 500);
        return false;
    });
  </script>

<script type="text/javascript" src="http://tools.iwillteachyoutoberich.com/iwtbounce/js/build/iwt_popup.js"></script>  

<script>  
    $(document).ready( function () {
      var popup = new IWTPopup('http://tools.iwillteachyoutoberich.com/iwtbounce/campaigns/iwt/ug-habits/', {
        cookieName: 'iwt_popup_ug-to-habits',
        params: {
          'utm_source': 'iwtbounce',  
          'utm_medium': 'iwtbounce',  
          'utm_campaign': 'iwtbounce-ug-to-habits',
          'brand': 'iwt',  
          'carrot_name': 'insider/ultimate-guide-to-habits-pdf/',
          'bonus_redirect': 'insider/ultimate-guide-to-habits-pdf/'
        }
      });
    }); 
</script>

  <!-- Please call pinit.js only once per page -->
  <script async data-pin-height="32" data-pin-hover="true" data-pin-shape="round" defer src="http://assets.pinterest.com/js/pinit.js"></script>

  <img style="opacity: 0; position: absolute; top: 0; left: -2000px;" src="http://www.iwillteachyoutoberich.com/guides/ultimate-guide-to-habits/assets/img/share-image.png" alt="The Ultimate Guide to Habits" />
  <script type="text/javascript">
var _vis_opt_queue = window._vis_opt_queue || [], _vis_counter = 0, _kmq = _kmq || [];
_vis_opt_queue.push(function() {
    try {
        if(!_vis_counter) {
            var _vis_data = {},_vis_combination,_vis_id,_vis_l=0;
            for(;_vis_l<_vwo_exp_ids.length;_vis_l++) {
                _vis_id = _vwo_exp_ids[_vis_l];
                if(_vwo_exp[_vis_id].ready) {
                    _vis_combination = _vis_opt_readCookie('_vis_opt_exp_'+_vis_id+'_combi');
                    if(typeof(_vwo_exp[_vis_id].combination_chosen) != "undefined")
                        _vis_combination = _vwo_exp[_vis_id].combination_chosen;
                    if(typeof(_vwo_exp[_vis_id].comb_n[_vis_combination]) != "undefined") {
                        _vis_data['VWO-Test-ID-'+_vis_id] = _vwo_exp[_vis_id].comb_n[_vis_combination];
                        _vis_counter++;
                    }
                }
            }
            // Use the _vis_data object created above to fetch the data, 
            // key of the object is the Test ID and the value is Variation Name
            if(_vis_counter) _kmq.push(['set', _vis_data]);
        }
    }
    catch(err) {};
});
</script>
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"40195a0833","applicationID":"8560361","transactionName":"b1NRMRdTChAEBRFaDlYZZhcMHQMWDAIAQE5NWkcMCFMQBkgBEFoFXRtHCkhaBQEMEhYcEVlER0hUHQ0NAQMdHRFQRg==","queueTime":0,"applicationTime":5,"atts":"QxRSR19JGR4=","errorBeacon":"bam.nr-data.net","agent":""}</script></body>
</html>
	

<?php wp_footer(); ?>
 <!-- ******************************************************************** -->
    <!-- * Custom Footer JavaScript Code ************************************ -->
    <!-- ******************************************************************** -->
    
    <?php if ( (isset($craiglistmarketingpro_options['footer_js'])) && ($craiglistmarketingpro_options['footer_js'] != "") ) : ?>
		<?php echo $craiglistmarketingpro_options['footer_js']; ?>
    <?php endif; ?>
</body>
</html>
